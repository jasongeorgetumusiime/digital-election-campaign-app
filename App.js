import React, {Component} from 'react';
import {Container} from 'native-base';
import {RootNavigator} from './app/routes';

class App extends Component {
  render() {
    const Nav = RootNavigator();

    return (
      <Container>
        <Nav />
      </Container>
    );
  }
}

export default App;
