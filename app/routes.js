import React from 'react';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';

// import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

// SCREENS

import SelectUser from './screens/auth/UserSelectScreen';
import AdminLogin from './screens/auth/AdminLoginScreen';
import UserLogin from './screens/auth/UserLoginScreen';
import FlagBearerLogin from './screens/auth/FlagBearerLoginScreen';
import UserRegistration from './screens/auth/UserRegistrationScreen';
import ContentList from './screens/content/ContentListScreen';
import UserContentList from './screens/content/UserContentList';
import AddFlagBearer from './screens/content/AddFlagBearer';
import ListFlagBearers from './screens/content/ListFlagBearers';
// import Logo from './utils/logo';

const headerConf = {
  defaultNavigationOptions: {
    headerTitleAlign: 'center',
    headerStyle: {
      backgroundColor: '#001338',
    },
    headerTintColor: 'white',
    // headerTitle: Logo,
  },
};

const AdminStack = createStackNavigator(
  {
    AdminLogin: AdminLogin,
    AddFlagBearer: AddFlagBearer,
    ListFlagBearers: ListFlagBearers,
  },
  {
    initialRouteName: 'AdminLogin',
    headerMode: 'none',
  },
);

const UserStack = createStackNavigator(
  {
    UserLogin: UserLogin,
    UserRegistration: UserRegistration,
    UserContentList: UserContentList,
  },
  {
    initialRouteName: 'UserLogin',
    headerMode: 'none',
  },
);

const FlagBearerStack = createStackNavigator(
  {
    SelectUser: SelectUser,
    FlagBearerLogin: FlagBearerLogin,
    ContentList: ContentList,
  },
  {
    initialRouteName: 'FlagBearerLogin',
    headerMode: 'none',
  },
);

const AuthStack = createStackNavigator(
  {
    SelectUser: SelectUser,
  },
  {
    headerMode: 'none',
  },
);

export const RootNavigator = () => {
  return createAppContainer(
    createSwitchNavigator(
      {
        Auth: AuthStack,
        FlagBearer: FlagBearerStack,
        User: UserStack,
        Admin: AdminStack,
      },
      {
        initialRouteName: 'Auth',
      },
    ),
  );
};
