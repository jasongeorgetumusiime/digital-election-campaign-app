import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Title,
  Picker,
  Footer,
  FooterTab,
} from 'native-base';
class AddFlagBearerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined,
    };
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#edb230'}}>
          <Left>
            <Button onPress={() => this.props.navigation.goBack()} transparent>
              <Icon name="arrow-back" style={{color: '#d9534f'}} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>New Flag Bearer</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="menu" style={{color: '#d9534f'}} />
            </Button>
          </Right>
        </Header>
        <Container style={styles.container}>
          <Form style={{alignItems: 'center', alignContent: 'center'}}>
            <Item rounded style={styles.textInput}>
              <Label>Full Name</Label>
              <Input />
            </Item>

            <Item rounded style={styles.textInput}>
              <Label>Flag Bearer ID</Label>
              <Input />
            </Item>

            <Item rounded style={styles.picker}>
              <Label>Political Party</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{width: undefined}}
                placeholder="Sign as"
                placeholderStyle={{color: '#bfc6ea'}}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}>
                <Picker.Item label="NRM" value="key0" />
                <Picker.Item label="People Power" value="key1" />
                <Picker.Item label="FDC" value="key2" />
                <Picker.Item label="DP" value="key3" />
                <Picker.Item label="Independent" value="key4" />
              </Picker>
            </Item>

            <Item rounded style={styles.picker}>
              <Label>Region</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{width: undefined}}
                placeholder="Sign as"
                placeholderStyle={{color: '#bfc6ea'}}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}>
                <Picker.Item label="National" value="key0" />
                <Picker.Item label="Kampala" value="key1" />
                <Picker.Item label="Masaka" value="key2" />
                <Picker.Item label="Gulu" value="key3" />
                <Picker.Item label="Entebbe" value="key4" />
              </Picker>
            </Item>

            <Button
              onPress={() => this.props.navigation.navigate('ListFlagBearers')}
              danger
              rounded
              style={styles.button}>
              <Text>Add Flag Bearer</Text>
            </Button>
          </Form>
        </Container>
        <Footer>
          <FooterTab style={styles.footerTab}>
            <Button vertical active style={{backgroundColor: '#ffd166'}}>
              <Ionicons
                name={'md-person-add-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Bearer</Text>
            </Button>
            <Button vertical>
              <Ionicons name={'md-list-outline'} size={25} color={'#d9534f'} />
              <Text style={styles.text}>Bearers</Text>
            </Button>
            {/* <Button vertical>
              <Ionicons
                name={'md-cloud-upload-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Upload</Text>
            </Button> */}
            <Button vertical>
              <Ionicons
                name={'ios-log-out-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Log Out</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edb230',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    margin: 20,
    width: '90%',
    height: 50,
    alignSelf: 'center',
    borderColor: '#fff',
  },
  title: {
    color: '#d9534f',
    fontFamily: 'YanoneKaffeesatz-Medium',
    alignSelf: 'center',
  },
  picker: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  textInput: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  footerTab: {
    backgroundColor: '#edb230',
  },
  text: {
    color: '#d9534f',
  },
});

export default AddFlagBearerScreen;
