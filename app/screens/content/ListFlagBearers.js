import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Container,
  Header,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Footer,
  FooterTab,
  Content,
  Icon,
  Title,
} from 'native-base';
export default class ListFlagBearers extends Component {
  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#edb230'}}>
          <Left>
            <Button onPress={() => this.props.navigation.goBack()} transparent>
              <Icon name="arrow-back" style={{color: '#d9534f'}} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Flag Bearers</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="menu" style={{color: '#d9534f'}} />
            </Button>
          </Right>
        </Header>

        <Content>
          <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  square
                  source={{
                    uri:
                      'https://www.aljazeera.com/wp-content/uploads/2016/06/6733efc9b4f3483a8a3e3036df2af852_18.jpeg?resize=770%2C513',
                  }}
                />
              </Left>
              <Body>
                <Text>His Excellence Yoweri Kaguta Museveni</Text>
                <Text note numberOfLines={1}>
                  NRM
                </Text>
              </Body>
              <Right>
                <Button transparent>
                  <Text style={styles.text}>View</Text>
                </Button>
              </Right>
            </ListItem>
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  square
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSXWDyb0fbHA4-REfv3PMwfLhRCCjGbbUu-cA&usqp=CAU',
                  }}
                />
              </Left>
              <Body>
                <Text>Kyagulanyi Ssentamu Bobi wine</Text>
                <Text note numberOfLines={1}>
                  People Power
                </Text>
              </Body>
              <Right>
                <Button transparent>
                  <Text style={styles.text}>View</Text>
                </Button>
              </Right>
            </ListItem>
          </List>
        </Content>
        <Footer>
          <FooterTab style={styles.footerTab}>
            <Button vertical active style={{backgroundColor: '#ffd166'}}>
              <Ionicons
                name={'md-person-add-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Bearer</Text>
            </Button>
            <Button vertical>
              <Ionicons name={'md-list-outline'} size={25} color={'#d9534f'} />
              <Text style={styles.text}>Bearers</Text>
            </Button>
            {/* <Button vertical>
              <Ionicons
                name={'md-cloud-upload-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Upload</Text>
            </Button> */}
            <Button vertical>
              <Ionicons
                name={'ios-log-out-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Log Out</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edb230',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    margin: 20,
    width: '90%',
    height: 50,
    alignSelf: 'center',
    borderColor: '#fff',
  },
  title: {
    color: '#d9534f',
    fontFamily: 'YanoneKaffeesatz-Medium',
    alignSelf: 'center',
  },
  picker: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  textInput: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  footerTab: {
    backgroundColor: '#edb230',
  },
  text: {
    color: '#d9534f',
  },
});
