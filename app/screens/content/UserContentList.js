import React, {Component} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {StyleSheet} from 'react-native';

import {
  Container,
  Header,
  Content,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Title,
  Footer,
  FooterTab,
  Text,
} from 'native-base';
import ContentItem from './ContentItem';
export default class UserContentListScreen extends Component {
  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#edb230'}}>
          <Left>
            <Button onPress={() => this.props.navigation.goBack()} transparent>
              <Icon name="arrow-back" style={{color: '#d9534f'}} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Feed</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="menu" style={{color: '#d9534f'}} />
            </Button>
          </Right>
        </Header>
        <Content>
          <ContentItem
            contentHeader="Will 2021 elections happen?"
            contentUri="https://www.independent.co.ug/wp-content/uploads/2020/06/2021-elections.jpg"
          />
          <ContentItem
            contentHeader="Poll says Museveni wouldn’t win election"
            contentUri="https://www.independent.co.ug/wp-content/uploads/2020/08/Museveni.jpg"
          />
          <ContentItem
            contentHeader="Youth elections in Kampala marred by chaos"
            contentUri="https://image.chitra.live/api/v1/wps/10afbec/8b7fe9c9-bf2f-47fe-9460-3b2709d1a1f2/1/WhatsApp-Image-2020-08-17-at-1-00-38-PM-671x403.jpeg"
          />
        </Content>
        <Footer>
          <FooterTab style={styles.footerTab}>
            <Button vertical>
              <Ionicons
                name={'md-list-circle-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Feed</Text>
            </Button>
            {/* <Button vertical>
              <Ionicons
                name={'md-cloud-upload-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Upload</Text>
            </Button> */}
            <Button vertical active style={{backgroundColor: '#ffd166'}}>
              <Ionicons
                name={'md-person-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Profile</Text>
            </Button>
            <Button vertical>
              <Ionicons
                name={'ios-log-out-outline'}
                size={25}
                color={'#d9534f'}
              />
              <Text style={styles.text}>Log Out</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  footerTab: {
    backgroundColor: '#edb230',
  },
  text: {
    color: '#d9534f',
  },
  title: {
    color: '#d9534f',
    fontFamily: 'YanoneKaffeesatz-Medium',
    alignSelf: 'center',
  },
});
