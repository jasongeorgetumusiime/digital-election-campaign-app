import React, {Component} from 'react';
import {Image, StyleSheet} from 'react-native';
import {
  Card,
  CardItem,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
} from 'native-base';
export default class ContentItem extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Card style={styles.card}>
        <CardItem>
          <Left>
            <Body>
              <Text>{this.props.contentHeader}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody>
          <Image
            source={{
              uri: this.props.contentUri,
            }}
            style={{height: 200, width: null, flex: 1}}
          />
        </CardItem>
        <CardItem>
          <Left>
            <Button transparent>
              <Icon active name="thumbs-up" style={styles.captionIcon} />
              <Text style={styles.captionItem}>12 Likes</Text>
            </Button>
          </Left>
          <Body>
            <Button transparent>
              <Icon active name="chatbubbles" style={styles.captionIcon} />
              <Text style={styles.captionItem}>4 comments</Text>
            </Button>
          </Body>
          <Right>
            <Text>11h ago</Text>
          </Right>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  captionItem: {
    color: '#d9534f',
    fontSize: 12,
    textAlign: 'center',
  },
  captionIcon: {
    color: '#d9534f',
    fontSize: 15,
  },
  card: {
    width: '99%',
    alignSelf: 'center',
  },
});
