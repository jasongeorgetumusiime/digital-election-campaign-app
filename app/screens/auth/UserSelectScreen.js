import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  Container,
  Form,
  Item,
  Picker,
  Icon,
  Label,
  Button,
  Text,
} from 'native-base';
import Logo from '../../utils/Logo';
export default class UserSelectScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: 'key1',
    };
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }

  pressedContinue = () => {
    switch (this.state.selected2) {
      case 'key1':
        this.props.navigation.navigate('FlagBearer');
        break;
      case 'key2':
        this.props.navigation.navigate('User');
        break;
      case 'key3':
        this.props.navigation.navigate('Admin');
        break;
      default:
        alert('Invalid selection.');
    }
  };

  render() {
    return (
      <Container style={styles.container}>
        <View style={styles.logo}>
          <Logo />
        </View>

        <View>
          <Text style={styles.heading}>MEDIA</Text>
        </View>

        <Form style={{alignItems: 'center'}}>
          <Item rounded style={styles.picker}>
            <Label>Select User</Label>

            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{width: undefined}}
              placeholderIconColor="#007aff"
              selectedValue={this.state.selected2}
              onValueChange={this.onValueChange2.bind(this)}>
              <Picker.Item label="Flag Bearer" value="key1" />
              <Picker.Item label="User" value="key2" />
              <Picker.Item label="Administrator" value="key3" />
            </Picker>
          </Item>

          <Button
            rounded
            danger
            onPress={this.pressedContinue}
            style={styles.button}>
            <Text>Continue</Text>
          </Button>
        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edb230',
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  logo: {
    padding: 0,
  },
  heading: {
    fontSize: 100,
    padding: 15,
    fontFamily: 'YanoneKaffeesatz-Regular',
    color: '#d9534f',
  },
  button: {
    margin: 20,
    width: '90%',
    height: 50,
    alignSelf: 'center',
    borderColor: '#fff',
  },
});
