import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Title,
  Picker,
} from 'native-base';
class UserRegistrationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined,
    };
  }
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#edb230'}}>
          <Left>
            <Button onPress={() => this.props.navigation.goBack()} transparent>
              <Icon name="arrow-back" style={{color: '#d9534f'}} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Register</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="menu" style={{color: '#d9534f'}} />
            </Button>
          </Right>
        </Header>
        <Container style={styles.container}>
          <Form style={{alignItems: 'center', alignContent: 'center'}}>
            <Item rounded style={styles.textInput}>
              <Label>Full Name</Label>
              <Input />
            </Item>

            <Item rounded style={styles.textInput}>
              <Label>Email Address</Label>
              <Input />
            </Item>

            <Item rounded style={styles.picker}>
              <Label>District of Birth</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{width: undefined}}
                placeholder="Sign as"
                placeholderStyle={{color: '#bfc6ea'}}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}>
                <Picker.Item label="Kampala" value="key0" />
                <Picker.Item label="Masaka" value="key1" />
                <Picker.Item label="Gulu" value="key2" />
                <Picker.Item label="Entebbe" value="key3" />
              </Picker>
            </Item>

            <Item rounded style={styles.picker}>
              <Label>District of Residence</Label>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{width: undefined}}
                placeholder="Sign as"
                placeholderStyle={{color: '#bfc6ea'}}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}>
                <Picker.Item label="Kampala" value="key0" />
                <Picker.Item label="Masaka" value="key1" />
                <Picker.Item label="Gulu" value="key2" />
                <Picker.Item label="Entebbe" value="key3" />
              </Picker>
            </Item>

            <Button
              onPress={() => {
                this.props.navigation.navigate('UserLogin');
              }}
              danger
              rounded
              style={styles.button}>
              <Text>Register</Text>
            </Button>
            <View>
              <Text style={styles.altLink}>Sign in</Text>
            </View>
          </Form>
        </Container>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edb230',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    margin: 20,
    width: '90%',
    height: 50,
    alignSelf: 'center',
    borderColor: '#fff',
  },
  title: {
    color: '#d9534f',
    fontFamily: 'YanoneKaffeesatz-Medium',
    alignSelf: 'center',
  },
  picker: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  textInput: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  altLink: {
    color: '#d9534f',
    fontFamily: 'YanoneKaffeesatz-Bold',
    marginTop: 20,
    fontSize: 23,
  },
});

export default UserRegistrationScreen;
