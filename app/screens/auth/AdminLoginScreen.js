import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Title,
} from 'native-base';
class AdminLoginScreen extends Component {
  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#edb230'}}>
          <Left>
            <Button
              onPress={() => this.props.navigation.navigate('SelectUser')}
              transparent>
              <Icon name="arrow-back" style={{color: '#d9534f'}} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.title}>Login</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="menu" style={{color: '#d9534f'}} />
            </Button>
          </Right>
        </Header>
        <Container style={styles.container}>
          <Form style={{alignItems: 'center', alignContent: 'center'}}>
            <Item rounded style={styles.textInput}>
              <Label>Username</Label>
              <Input />
            </Item>

            <Item rounded style={styles.textInput}>
              <Label>Password</Label>
              <Input />
            </Item>

            <Button
              rounded
              danger
              onPress={() => this.props.navigation.navigate('AddFlagBearer')}
              style={{
                margin: 10,
                width: '90%',
                height: 50,
                alignSelf: 'center',
              }}>
              <Text>Sign In</Text>
            </Button>
          </Form>
        </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edb230',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    margin: 20,
    width: '90%',
    height: 50,
    alignSelf: 'center',
    borderColor: '#fff',
  },
  title: {
    color: '#d9534f',
    fontFamily: 'YanoneKaffeesatz-Medium',
    alignSelf: 'center',
  },
  textInput: {
    paddingLeft: 10,
    margin: 10,
    width: '90%',
    height: 50,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
});

export default AdminLoginScreen;
